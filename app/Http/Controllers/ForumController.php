<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Forum;
use App\Http\Requests\ForumPost as ForumPost;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Str;

class ForumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        



    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ForumPost $request)
    {

           $user = Auth::user();
           // dd($user->id);
           // dd($request->hasFile('image'));
           // dd();
        if($request->hasFile('image')){
        $thumb =\Str::random(20).'.'.$request->file('image')->extension();
        $imagename = Str::of(\Hash::make($request->file('image')->getClientOriginalName()).'.'.$request->file('image')->extension())->replace('/','');

        $thumbpath = ('thumb/'. $imagename);


         $image=    $request->file('image')->store('questions');
         // dd($image);
        $link = ('../storage/app/'.$image);
        // var_dump($link);
        // dd(file_exists($link));
         $resize = \Image::make($link)->resize(100,100);
         // dd($resize);
          $r = Storage::put("/thumb/". $imagename,  $resize->encode('jpg'));
          // dd("/thumb/". $imagename);
          echo $imagename;
         // $resize->save('../storage/app/'.($thumbpath), 60);

  // dd( $resize->response('jpg'));
        }else{
            $image="";
             $thumbpath="";
        }
        $forum  = new Forum();
        $forum->user_id = $user->id;
        $forum->title = $request->title;
        $forum->question = $request->question;
        $forum->image = $image;
        $forum->thumb =  $thumbpath;

        $forum->save();

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id=null)
    {   
     $pagenum = 6;
        if ($id == 'show'){
            $forum = Forum::with('User')->orderBy('created_at','desc')->paginate($pagenum);
        }else{
            $forum = Forum::with('User')->where('id',$id)->orderBy('created_at','desc')->paginate($pagenum);
        }
        
        return $forum;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        // dd($request->id);

        $forum = Forum::with('User')->findorfail($request->id);
        $forum->title= $request->title;
        $forum->question= $request->question;
        $forum->save();
        return $forum;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $forum  = Forum::findorfail($id);
       $imgpath=(public_path().'/'.$forum->image);
       \File::delete($imgpath);
        $imgpath=(public_path().'/'.$forum->thumb);
       \File::delete($imgpath);
       $forum->delete();
      
    }
}
